package me.Leftwitch.PetBlocks.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class MySQL {
	public static Connection	con		= null;
	public static String		dbHost	= "localhost";	// Hostname
	public static String		dbPort	= "3306";		// Port -- Standard:
														// 3306
	public static String		dbName	= "petblocks";	// Datenbankname
	public static String		dbUser	= "root";		// Datenbankuser
	public static String		dbPass	= "";			// Datenbankpasswort
	// support_tickets id INT, date BIGINT,commiter TEXT,status INT,text
	// TEXT,supporters TEXT

	public static void connect() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?" + "user="
					+ dbUser + "&" + "password=" + dbPass);
			generateTables();
		} catch (ClassNotFoundException e) {
			System.out.println("Couldn't load driver");
		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	public static void generateTables() {
		try {
			con.prepareStatement(
					"CREATE TABLE IF NOT EXISTS `petblocks_owned` ( `id` INT NOT NULL AUTO_INCREMENT , `UUID` TEXT NOT NULL , `pet` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;")
					.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub

	}

	public static boolean hasPet(UUID uid, byte pet) {
		try {
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM `petblocks_owned` WHERE `UUID` = ?");
			stmt.setString(1, uid.toString());
			ResultSet set = stmt.executeQuery();
			while (set.next()) {
				if (set.getInt("pet") == pet)
					return true;
			}
		} catch (Exception error) {
			error.printStackTrace();
		}
		return false;
	}
	
	public static void addPet(UUID uid,byte pet){
		try {
			PreparedStatement stmt = con.prepareStatement("INSERT INTO `petblocks_owned` (`id`, `UUID`, `pet`) VALUES (NULL, '"+uid.toString()+"', '"+pet+"');");
			stmt.executeUpdate();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

}
