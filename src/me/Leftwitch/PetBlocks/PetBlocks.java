package me.Leftwitch.PetBlocks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.Leftwitch.PetBlocks.Commands.AddPetCommand;
import me.Leftwitch.PetBlocks.Commands.OpenGuiCommands;
import me.Leftwitch.PetBlocks.Config.MobConfig;
import me.Leftwitch.PetBlocks.Config.MySQLConfig;
import me.Leftwitch.PetBlocks.Entities.CustomArmorStand;
import me.Leftwitch.PetBlocks.Entities.CustomEntityRegistry;
import me.Leftwitch.PetBlocks.Entities.CustomZombie;
import me.Leftwitch.PetBlocks.Entities.ShearsEntity;
import me.Leftwitch.PetBlocks.MySQL.MySQL;
import net.minecraft.server.v1_11_R1.Entity;

public class PetBlocks extends JavaPlugin {

	public static HashMap<Player, ShearsEntity>	pets	= Maps.newHashMap();
	public static Gson							gson	= new GsonBuilder().disableHtmlEscaping().setPrettyPrinting()
			.enableComplexMapKeySerialization().create();
	public static File							storageFile;
	public static File							configFile;
	public static List<MobConfig>				mobs;
	public static MySQLConfig					sqlConfig;

	public void onEnable() {
		storageFile = new File(getDataFolder(), "mobs.conf");
		configFile = new File(getDataFolder(), "config.conf");
		loadConfig();
		if(sqlConfig==null)
			sqlConfig=new MySQLConfig("localhost", "3306", "petblocks", "root", "");
		updateConfig();

		MySQL.dbHost=sqlConfig.getDbHost();
		MySQL.dbName=sqlConfig.getDbName();
		MySQL.dbPass=sqlConfig.getDbPass();
		MySQL.dbPort=sqlConfig.getDbPort();
		MySQL.dbUser=sqlConfig.getDbUser();

		
		MySQL.connect();
		Bukkit.getPluginManager().registerEvents(new SimplePetListener(), this);
		CustomEntityRegistry.registerCustomEntity(54, "zombie", CustomZombie.class);
		CustomEntityRegistry.registerCustomEntity(30, "armor_stand", CustomArmorStand.class);

		getCommand("petblockgui").setExecutor(new OpenGuiCommands());
		getCommand("addpetblock").setExecutor(new AddPetCommand());

		loadMobs();
		if (mobs == null) {
			mobs = Lists.newArrayList(
					new MobConfig("&bSoldier", (byte) 10, Lists.newArrayList("&9Protects you, and your pizza!")),
					new MobConfig("&cCute red thing", (byte) 5, Lists.newArrayList("&9Well, its red.")));
			updateMobs();
		}

	}

	public void onDisable() {
		for (Player p : pets.keySet()) {
			pets.get(p).remove();
		}
	}

	public static void spawnEntity(Entity entity, Location loc) {
		entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw() + 180, loc.getPitch());
		((CraftWorld) loc.getWorld()).getHandle().addEntity(entity, SpawnReason.CUSTOM);
	}

	public static void updateMobs() {
		List<String> lines = Arrays.asList(gson.toJson(mobs));
		Path file = Paths.get(storageFile.toPath().toString());
		try {
			storageFile.getParentFile().mkdirs();
			storageFile.createNewFile();
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void updateConfig() {
		List<String> lines = Arrays.asList(gson.toJson(sqlConfig));
		Path file = Paths.get(configFile.toPath().toString());
		try {
			configFile.getParentFile().mkdirs();
			configFile.createNewFile();
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Player getOwner(ShearsEntity en) {
		for (Player ps : pets.keySet()) {
			if (pets.get(ps).equals(en)) {
				return ps;
			}
		}
		return null;
	}

	public static Player getOwner(CustomArmorStand en) {
		for (Player ps : pets.keySet()) {
			if (pets.get(ps).shears != null && pets.get(ps).shears.equals(en)) {
				return ps;
			}
		}
		return null;
	}

	public static Player getOwner(CustomZombie en) {
		for (Player ps : pets.keySet()) {
			if (pets.get(ps).ki != null && pets.get(ps).ki.equals(en)) {
				return ps;
			}
		}
		return null;
	}

	public void loadMobs() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(storageFile));
			MobConfig[] crates = gson.fromJson(reader, MobConfig[].class);
			this.mobs = new LinkedList<MobConfig>(Arrays.asList(crates));

		} catch (FileNotFoundException ex) {
		}

	}

	public void loadConfig() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(configFile));
			this.sqlConfig = gson.fromJson(reader, MySQLConfig.class);

		} catch (FileNotFoundException ex) {
		}
	}

	public static void openGui(Player p) {
		Inventory inv = Bukkit.createInventory(null, 9 * 5, "�aPets");
		for (MobConfig mc : mobs) {
			ItemStack is = new ItemStack(Material.SHEARS, 1, mc.getDataValue());
			ItemMeta meta = is.getItemMeta();
			meta.addItemFlags(ItemFlag.values());
			meta.spigot().setUnbreakable(true);
			meta.setDisplayName(mc.getName().replace("&", "�"));
			List<String> newLore = Lists.newArrayList();
			for (String s : mc.getLore())
				newLore.add(s.replace("&", "�"));
			newLore.add(ownPet(p, mc) ? "�aPet owned!" : "�cPet not owned!");
			meta.setLore(newLore);
			is.setItemMeta(meta);
			inv.addItem(is);
		}
		p.openInventory(inv);
	}

	public static void openSettings(Player p) {
		Inventory settings = Bukkit.createInventory(null, 9, "�aPet Settings");
		ItemStack ridePet = new ItemStack(Material.SADDLE);
		ItemMeta metaRide = ridePet.getItemMeta();
		metaRide.addItemFlags(ItemFlag.values());
		metaRide.setDisplayName("�aRide Pet");
		ridePet.setItemMeta(metaRide);

		ItemStack remove = new ItemStack(Material.BARRIER);
		ItemMeta rmMeta = remove.getItemMeta();
		rmMeta.setDisplayName("�cRemove Pet");
		rmMeta.addItemFlags(ItemFlag.values());
		remove.setItemMeta(rmMeta);

		settings.setItem(0, ridePet);
		settings.setItem(8, remove);
		p.openInventory(settings);
	}

	public static void selectPet(Player p, ShearsEntity en) {
		if (pets.containsKey(p)) {
			pets.get(p).remove();
			pets.remove(p);
		}
		pets.put(p, en);

	}

	public static boolean ownPet(Player p, MobConfig config) {
		return MySQL.hasPet(p.getUniqueId(), config.getDataValue());
	}
}
