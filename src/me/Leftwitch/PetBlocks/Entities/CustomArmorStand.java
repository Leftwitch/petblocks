package me.Leftwitch.PetBlocks.Entities;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.minecraft.server.v1_11_R1.Entity;
import net.minecraft.server.v1_11_R1.EntityArmorStand;
import net.minecraft.server.v1_11_R1.EntityLiving;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_11_R1.World;

public class CustomArmorStand extends EntityArmorStand {

	public CustomZombie	addicted;
	public Entity		riding;

	public CustomArmorStand(World world, byte id) {
		super(world);
		ItemStack is = new ItemStack(Material.SHEARS, 1, id);
		ItemMeta meta = is.getItemMeta();
		meta.spigot().setUnbreakable(true);
		is.setItemMeta(meta);
		((ArmorStand) getBukkitEntity()).setHelmet(is);
		setInvisible(true);
		setSmall(true);
	}

	public CustomArmorStand(org.bukkit.World world, byte id) {
		this(((CraftWorld) world).getHandle(), id);
	}

	@Override
	public boolean startRiding(Entity en) {
		this.riding = en;
		return true;
	}

	@Override
	public void stopRiding() {
		riding = null;
		// super.stopRiding();
	}

	@Override
	public void A_() {
		// Bukkit.broadcastMessage("TICK");
		if (passengers.size() != 0) {
			if (isNoGravity())
				setNoGravity(false);
			super.A_();
			return;
		} else {
			if (!isNoGravity())
				setNoGravity(true);
		}
		if (riding == null)
			return;
		setPositionRotation(riding.locX, riding.locY + 0.3, riding.locZ, riding.yaw, riding.pitch);
		for (Player ps : Bukkit.getOnlinePlayers()) {
			CraftPlayer cp = (CraftPlayer) ps;
			cp.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityTeleport(this));
		}
	}

	public void setYawPitch(float yaw, float pitch) {
		super.setYawPitch(yaw, pitch);
	}

	// This is the update method of the entity

	@Override
	public void g(float sideMot, float forMot) {
		float speed = 0.2F;
		this.l(speed);

		if (this.passengers != null && this.passengers.size() > 0) {
			EntityLiving passenger = (EntityLiving) this.passengers.get(0);
			this.yaw = passenger.yaw;
			this.lastYaw = this.yaw;
			this.pitch = (passenger.pitch * 0.5F);
			setYawPitch(this.yaw, this.pitch);
			this.aN = this.yaw;
			this.aP = this.aN;

			Float speedmultiplicator = 3F;// Here you can set the speed
			sideMot = passenger.be * speedmultiplicator;
			forMot = passenger.bf * speedmultiplicator;
			if (forMot <= 0.0F) {
				forMot *= 0.25F;// Make backwards slower
			}
			Field jump = null; // Jumping
			try {
				jump = EntityLiving.class.getDeclaredField("bd");
			} catch (NoSuchFieldException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			jump.setAccessible(true);

			if (jump != null && this.onGround) { // Wouldn't want it jumping
													// while on the ground would
													// we?
				try {
					if (jump.getBoolean(passenger)) {
						double jumpHeight = 0.5D; // Here you can set the
													// jumpHeight
						this.motY = jumpHeight; // Used all the time in NMS for
												// entity jumping
					}
					jump.set(this, jump.getBoolean(passenger));
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			super.g(sideMot, forMot);
		}
	}

	public static Object getPrivateField(String fieldName, Class clazz, Object object) {
		Field field;
		Object o = null;
		try {
			field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			o = field.get(object);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return o;
	}
}
