package me.Leftwitch.PetBlocks.Entities;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import me.Leftwitch.PetBlocks.ShearsPet;
import net.minecraft.server.v1_11_R1.Entity;

public class ShearsEntity implements ShearsPet {

	public CustomArmorStand	shears;
	public CustomZombie		ki;
	public byte id;
	@Override
	public void setPassenger(Player p) {
		if (ki != null && ki.isAlive())
			ki.getBukkitEntity().remove();
		shears.stopRiding();
		if (((CraftPlayer) p).getHandle().passengers.size() == 0)
			((CraftPlayer) p).getHandle().startRiding(shears);

		// TODO Auto-generated method stub

	}

	@Override
	public void startFollowing(Player p) {
		if (ki == null) {
			ki = new CustomZombie(p.getWorld(), p);
		}
		spawnEntity(ki, p.getLocation());
		if (shears.passengers.size() != 0) {
			for (Entity en : shears.passengers) {
				en.stopRiding();
			}
		}

		shears.startRiding(ki);
		// TODO Auto-generated method stub

	}

	@Override
	public void remove() {
		if (ki != null && ki.isAlive())
			ki.getBukkitEntity().remove();
		shears.getBukkitEntity().remove();
		// TODO Auto-generated method stub

	}

	@Override
	public void spawn(Player p, byte id) {
		if(id!=-1)
		this.id=id;
		ki = new CustomZombie(p.getWorld(), p);
		shears = new CustomArmorStand(p.getWorld(), this.id);

		spawnEntity(ki, p.getLocation());
		spawnEntity(shears, p.getLocation());
		// TODO Auto-generated method stub

	}

	@Override
	public void placeOnPlayer(Player p) {
		if (ki != null && ki.isAlive())
			ki.getBukkitEntity().remove();

		shears.startRiding(((CraftPlayer) p).getHandle());

		// TODO Auto-generated method stub

	}

	public static void spawnEntity(Entity entity, Location loc) {
		entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw() + 180, loc.getPitch());
		((CraftWorld) loc.getWorld()).getHandle().addEntity(entity, SpawnReason.CUSTOM);
	}

}
