package me.Leftwitch.PetBlocks.Entities;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_11_R1.Entity;
import net.minecraft.server.v1_11_R1.EntityInsentient;
import net.minecraft.server.v1_11_R1.NBTTagCompound;
import net.minecraft.server.v1_11_R1.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_11_R1.PathEntity;
import net.minecraft.server.v1_11_R1.PathfinderGoal;

public final class OwnerPathfinder extends PathfinderGoal {
	private final EntityInsentient entity;
	private PathEntity path;
	private final Player player;

	private int counter2;
	private int counter;

	public OwnerPathfinder(EntityInsentient entitycreature, Player player) {
		this.entity = entitycreature;
		this.player = player;
	}

	@Override
	public boolean a() {
		if (this.player == null) {
			return this.path != null;
		}

		if (!this.entity.getWorld().getWorldData().getName().equals(this.player.getWorld().getName())) {
			if (this.entity.passengers.size() > 0) {
				Entity pass = this.entity.passengers.get(0);
				pass.stopRiding();
				this.entity.passengers.remove(pass);
				this.entity.getBukkitEntity().teleport(this.player.getLocation());
				pass.getBukkitEntity().teleport(this.player.getLocation());
				pass.startRiding(this.entity);
				this.entity.passengers.add(pass);
			} else {
				this.entity.getBukkitEntity().teleport(this.player.getLocation());
			}
		} 
		if (this.entity.getBukkitEntity().getLocation().distance(this.player.getLocation()) > 2) {
            Location targetLocation = this.player.getLocation();
            this.path = this.entity.getNavigation().a(targetLocation.getX() + 1, targetLocation.getY(), targetLocation.getZ() + 1);
			// if(this.entity.passengers.size()>0){
			// ((CustomArmorStand)this.entity.passengers.get(0)).setHeadPose(new
			// Vector3f(player.getLocation().getPitch(),this.player.getLocation().getYaw()-90,0));
			// }
			if (this.entity.getBukkitEntity().getLocation().distance(this.player.getLocation()) > 15)
				if (this.entity.passengers.size() > 0) {
					Entity pass = this.entity.passengers.get(0);
					pass.stopRiding();
					this.entity.passengers.remove(pass);
					this.entity.getBukkitEntity().teleport(this.player.getLocation());
					pass.getBukkitEntity().teleport(this.player.getLocation());
					pass.startRiding(this.entity);
					this.entity.passengers.add(pass);
				} else {
					this.entity.getBukkitEntity().teleport(this.player.getLocation());
				}
		

		}
		
		
		if (this.path != null) {
			this.c();
		}
		
		return this.path != null;
	}

	@Override
	public void c() {
		if (this.entity instanceof CustomZombie)
			this.entity.getNavigation().a(this.path, 2.5D);
		else
			this.entity.getNavigation().a(this.path, 1D);
	}

	
}