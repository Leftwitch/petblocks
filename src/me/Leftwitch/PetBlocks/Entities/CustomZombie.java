package me.Leftwitch.PetBlocks.Entities;

import java.lang.reflect.Field;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_11_R1.EntityZombie;
import net.minecraft.server.v1_11_R1.GenericAttributes;
import net.minecraft.server.v1_11_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_11_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_11_R1.SoundEffect;

public class CustomZombie extends EntityZombie {

	public Player owner;
	public CustomZombie(org.bukkit.World world, Player player) {
		super(((CraftWorld) world).getHandle());
		Set goalB = (Set) getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
		goalB.clear();
		Set goalC = (Set) getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
		goalC.clear();
		Set targetB = (Set) getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
		targetB.clear();
		Set targetC = (Set) getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
		targetC.clear();

		// this.goalSelector.a(0, new PathfinderGoalFloat(this));
		// this.goalSelector.a(5, new PathfinderGoalMoveTowardsRestriction(this,
		// 1.0D));
		// this.goalSelector.a(6, new PathfinderGoalMoveThroughVillage(this,
		// 1.0D, false));
		// this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, 1.0D));
		// this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this,
		// EntityHuman.class, 8.0F));
		// this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
		// this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, true));
		setBaby(true);

		this.goalSelector.a(0, new PathfinderGoalFloat(this));
		this.goalSelector.a(1, new OwnerPathfinder(this, player));
		this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.18D);
		((Zombie)getBukkitEntity()).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999999, 1));
		setSilent(true);
		collides=false;
		this.owner=player;
		setInvulnerable(true);
		// // This are its default goals.
	}

	@Override
	public void g(float sideMot, float forMot) {
		float speed = 0.2F;
		this.l(speed);
		super.g(sideMot, forMot);

	}

	public void setYawPitch(float yaw, float pitch) {
		super.setYawPitch(yaw, pitch);
	}

	public static Object getPrivateField(String fieldName, Class clazz, Object object) {
		Field field;
		Object o = null;
		try {
			field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			o = field.get(object);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return o;
	}

	@Override
	public void a(SoundEffect soundeffect, float f, float f1) {
	}
}
