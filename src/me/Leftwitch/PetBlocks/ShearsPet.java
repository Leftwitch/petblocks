package me.Leftwitch.PetBlocks;

import org.bukkit.entity.Player;

public interface ShearsPet {

	public void setPassenger(Player p);
	public void startFollowing(Player p);
	public void remove();
	public void spawn(Player p,byte id);
	public void placeOnPlayer(Player p);
}
