package me.Leftwitch.PetBlocks.Commands;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import me.Leftwitch.PetBlocks.MySQL.MySQL;

public class AddPetCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {

		if(!(sender instanceof ConsoleCommandSender)){
			sender.sendMessage("�cConsole Only!");
			return true;
		}
		
		OfflinePlayer of = Bukkit.getOfflinePlayer(args[0]);
		if(of==null)return true;
		UUID uid = of.getUniqueId();
		int pet = Integer.parseInt(args[1]);
		
		
		MySQL.addPet(uid, (byte)pet);
		sender.sendMessage("Added Pet!");
		return true;
	}

}
