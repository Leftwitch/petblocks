package me.Leftwitch.PetBlocks.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Leftwitch.PetBlocks.PetBlocks;

public class OpenGuiCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("§cIngame Only!");
			return true;
		}

		PetBlocks.openGui((Player) sender);
		return true;

	}

}
