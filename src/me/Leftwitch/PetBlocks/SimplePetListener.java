package me.Leftwitch.PetBlocks;

import org.bukkit.craftbukkit.v1_11_R1.entity.CraftArmorStand;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftZombie;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.spigotmc.event.entity.EntityDismountEvent;

import me.Leftwitch.PetBlocks.Config.MobConfig;
import me.Leftwitch.PetBlocks.Entities.CustomArmorStand;
import me.Leftwitch.PetBlocks.Entities.CustomZombie;
import me.Leftwitch.PetBlocks.Entities.ShearsEntity;

public class SimplePetListener implements Listener {
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (!PetBlocks.pets.containsKey(e.getPlayer()))
			return;
		PetBlocks.pets.get(e.getPlayer()).remove();
		PetBlocks.pets.remove(e.getPlayer());
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null)
			return;
		if (e.getClickedInventory().getTitle().equalsIgnoreCase("�aPets")) {
			if (e.getCurrentItem() == null || e.getCurrentItem().getTypeId() == 0)
				return;

			MobConfig conf = null;
			for (MobConfig c : PetBlocks.mobs) {
				if (c.getDataValue() == e.getCurrentItem().getDurability()) {
					conf = c;
					break;
				}
			}
			if (!PetBlocks.ownPet((Player) e.getWhoClicked(), conf)) {
				e.getWhoClicked().sendMessage("�cyou don't own this pet!");
				e.getWhoClicked().closeInventory();
				e.setCancelled(true);
				return;
			}
			ShearsEntity en = new ShearsEntity();
			en.spawn((Player) e.getWhoClicked(), conf.getDataValue());
			en.startFollowing((Player) e.getWhoClicked());
			PetBlocks.selectPet((Player) e.getWhoClicked(), en);

			e.getWhoClicked().closeInventory();
			e.setCancelled(true);

		}
	}

	@EventHandler
	public void onClickSettings(InventoryClickEvent e) {
		if (e.getClickedInventory() == null)
			return;
		if (e.getClickedInventory().getTitle().equalsIgnoreCase("�aPet Settings")) {
			if (e.getCurrentItem() == null || e.getCurrentItem().getTypeId() == 0)
				return;
			Player p = (Player) e.getWhoClicked();

			if (e.getSlot() == 0) {
				PetBlocks.pets.get(p).setPassenger(p);

			} else if (e.getSlot() == 8) {
				PetBlocks.pets.get(p).remove();
				PetBlocks.pets.remove(p);
			}

			e.getWhoClicked().closeInventory();
			e.setCancelled(true);

		}
	}

	@EventHandler
	public void onOpenSettings(PlayerInteractEntityEvent e) {
		if (e.getRightClicked().getType() != EntityType.ZOMBIE)
			return;
		CraftZombie zomb = (CraftZombie) e.getRightClicked();
		if (zomb.getHandle() instanceof CustomZombie) {
			if (PetBlocks.getOwner((CustomZombie) zomb.getHandle()) == e.getPlayer()) {
				PetBlocks.openSettings(e.getPlayer());
			}

		}
	}

	@EventHandler
	public void onLeaveVehicle(EntityDismountEvent e) {
		if (e.getDismounted().getType() != EntityType.ARMOR_STAND)
			return;
		CraftArmorStand stand = (CraftArmorStand) e.getDismounted();
		if (stand.getHandle() instanceof CustomArmorStand) {
			if (PetBlocks.getOwner((CustomArmorStand) stand.getHandle()) == (Player) e.getEntity()) {
				byte id = PetBlocks.pets.get((Player) e.getEntity()).id;

				ShearsEntity en = new ShearsEntity();
				en.spawn((Player) e.getEntity(), id);
				en.startFollowing((Player) e.getEntity());
				PetBlocks.selectPet((Player) e.getEntity(), en);

			}

		}
	}

	@EventHandler
	public void onMinipulate(PlayerArmorStandManipulateEvent e) {
		if (e.getRightClicked().getType() != EntityType.ARMOR_STAND)
			return;
		CraftArmorStand stand = (CraftArmorStand) e.getRightClicked();
		if (stand.getHandle() instanceof CustomArmorStand) {
			if (PetBlocks.getOwner((CustomArmorStand) stand.getHandle()) == e.getPlayer()) {
				PetBlocks.openSettings(e.getPlayer());
			}
			e.setCancelled(true);

		}
	}

}
