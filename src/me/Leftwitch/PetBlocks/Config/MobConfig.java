package me.Leftwitch.PetBlocks.Config;

import java.util.List;

public class MobConfig {

	String name;
	byte dataValue;
	List<String> lore;
	public MobConfig(String name, byte dataValue, List<String> lore) {
		super();
		this.name = name;
		this.dataValue = dataValue;
		this.lore = lore;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte getDataValue() {
		return dataValue;
	}
	public void setDataValue(byte dataValue) {
		this.dataValue = dataValue;
	}
	public List<String> getLore() {
		return lore;
	}
	public void setLore(List<String> lore) {
		this.lore = lore;
	}

}
