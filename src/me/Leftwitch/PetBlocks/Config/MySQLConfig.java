package me.Leftwitch.PetBlocks.Config;

public class MySQLConfig {

	String	dbHost	= "localhost";	// Hostname
	String	dbPort	= "3306";		// Port -- Standard:
									// 3306
	String	dbName	= "petblocks";	// Datenbankname
	String	dbUser	= "root";		// Datenbankuser
	String	dbPass	= "";			// Datenbankpasswort

	public MySQLConfig(String dbHost, String dbPort, String dbName, String dbUser, String dbPass) {
		super();
		this.dbHost = dbHost;
		this.dbPort = dbPort;
		this.dbName = dbName;
		this.dbUser = dbUser;
		this.dbPass = dbPass;
	}

	public String getDbHost() {
		return dbHost;
	}

	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}

	public String getDbPort() {
		return dbPort;
	}

	public void setDbPort(String dbPort) {
		this.dbPort = dbPort;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPass() {
		return dbPass;
	}

	public void setDbPass(String dbPass) {
		this.dbPass = dbPass;
	}
}
